import AVFoundation
import UIKit

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    let defaults = UserDefaults.standard
    var link:String = ""
    var password : String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        link = defaults.string(forKey: "link") ?? ""
        password = defaults.string(forKey: "password") ?? ""
        
        if (link == nil || link ==  "") {
            link = ""
        }
        if (password == nil) {
            password = ""
        }

        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }

    func found(code: String) {
        var firstchar = code.first
        if (firstchar == "#") {
            let components = code.components(separatedBy: "#")
            let fetchedLink = components[1]
            let fetechedPassword = components[2]
            print("Feteched link : " + fetchedLink)
            print("Fetched password : " + fetechedPassword)
            defaults.set(fetchedLink, forKey: "link")
            defaults.set(fetechedPassword,forKey: "password")
            link = fetchedLink
            password = fetechedPassword
        
            
            let controller = UIAlertController(title: "Settings Changed", message: "New server address and password changed" , preferredStyle: .alert)
            let okAction = UIAlertAction(title: "好的", style: .default) { (_) in
                self.captureSession.startRunning()
            }
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
            
        }
        else{
            sendHTTP(code: code)
        }
    }
    
    func sendHTTP (code:String){
        var errorflag:Bool = false
        let urlstring:String = ""
        let url = URL(string:urlstring)
        print(url)
        guard let requestUrl = url else { fatalError() }
        // Prepare URL Request Object
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "POST"
         
        // HTTP Request Parameters which will be sent in HTTP Request Body
        let postString = "";
        // Set HTTP Request Body
        request.httpBody = postString.data(using: String.Encoding.utf8);
        // Perform HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                // Check for Error
                if let error = error {
                    print("Error took place \(error)")
                    errorflag = true
                    return
                }
         
                // Convert HTTP Response Data to a String
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("Response data string:\n \(dataString)")
                    if(dataString=="1"){
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                    errorflag = false
                }
        }
        task.resume()
        if (errorflag == true){
            let controller = UIAlertController(title: "Internet error", message: "Failed to add record, please fix the internet connection and try again", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default){ (_) in self.captureSession.startRunning()}
            
            controller.addAction(okAction)
            present(controller, animated: true, completion: nil)
        }
        else{
            self.captureSession.startRunning()
        }
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
