# Athlete Management Application QR



## Senario

A local school aims to streamline their athlete management process during a major school sports event. During their cross-country running event, they encountered challenges in efficiently tracking the sequence and identity of students along the track and at the destination. In the past, student helpers visually trace them and write the arrival sequence on a paper, leading to potential inaccuracies and delays.

## Solution (Part 1 -- this app)
As part of the enhanced system, each student's name plate now includes a QR code containing their participant ID. This app offers the following features:

As a part of the solution, this app has the feature of :
- 1. Scan and Read Participant ID: The app allows for scanning the QR code to extract the participant's ID.
- 2. URL Embedding: The extracted ID is then embedded in a URL.
- 3. Database Integration: The app securely sends the extracted ID through an API to update the database.

In the improved system, students simply hold up their phones at the destination. As students reach the destination, they form a queue and have their QR codes scanned individually. This streamlined process ensures accurate recording of the approximate completion time for each student.

By leveraging QR code technology and this user-friendly app, the athlete management process becomes more efficient, saving time and improving tracking accuracy.
